;;; Lisparuga
;;; Copyright © 2020 David Thompson <dthompson2@worcester.edu>
;;;
;;; Lisparuga is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published
;;; by the Free Software Foundation, either version 3 of the License,
;;; or (at your option) any later version.
;;;
;;; Lisparuga is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Lisparuga.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; Game container.  Handles all the state, logic, and rendering for
;; the game itself.
;;
;;; Code:

(define-module (lisparuga game)
  #:use-module (chickadee)
  #:use-module (chickadee math)
  #:use-module (chickadee math easings)
  #:use-module (chickadee math rect)
  #:use-module (chickadee math vector)
  #:use-module (chickadee render color)
  #:use-module (chickadee render particles)
  #:use-module (chickadee render texture)
  #:use-module (chickadee scripting)
  #:use-module (ice-9 format)
  #:use-module (ice-9 match)
  #:use-module (lisparuga actor)
  #:use-module (lisparuga asset)
  #:use-module (lisparuga bullets)
  #:use-module (lisparuga config)
  #:use-module (lisparuga enemy)
  #:use-module (lisparuga node)
  #:use-module (lisparuga node-2d)
  #:use-module (lisparuga player)
  #:use-module (oop goops)
  #:export (<game>
            steer-player
            start-player-shooting
            stop-player-shooting
            toggle-player-polarity
            fire-player-homing-missiles
            spawn-enemies
            start-stage
            stop-stage
            game-over?
            complete?))

;;(define-asset clouds (load-image (scope-asset "images/clouds.png")))
(define-asset player-bullet-atlas
  (load-tile-atlas (scope-asset "images/player-bullets.png") 16 16))
(define-asset enemy-bullet-atlas
  (load-tile-atlas (scope-asset "images/enemy-bullets.png") 24 24))
(define-asset explosion-texture
  (load-image (scope-asset "images/explosion.png")))

;; nodes needed:
;; scrolling background
(define-class <game> (<canvas>)
  (player-control? #:accessor player-control? #:init-value #f)
  (complete? #:accessor complete? #:init-value #f)
  (skip-tutorial? #:accessor skip-tutorial? #:init-value #f)
  (stage-script #:accessor stage-script #:init-value #f))

(define-method (reset (game <game>))
  (set! (player-control? game) #f)
  (set! (complete? game) #f)
  (reset (& game player))
  (for-each detach (children (& game enemies)))
  (let ((battle-report (& game battle-report)))
    (and battle-report (detach battle-report))))

(define-method (initialize (game <game>) initargs)
  (next-method)
  (set! (views game)
    ;; Game happens on a 160x240 pixel screen.
    (list (make <view-2d>
            #:camera (make <camera-2d>
                       #:width 160
                       #:height 240)
            #:area (make-rect 80 0 160 240)
            #:clear-color (make-color 0.0 0.0 0.0 1.0)))))

(define-method (on-boot (game <game>))
  (let* ((player-bullets (make <bullet-field>
                           #:name 'player-bullets
                           #:rank 2
                           #:capacity 500
                           #:texture-atlas player-bullet-atlas))
         (player (make-player player-bullets))
         (enemy-bullets (make <bullet-field>
                          #:name 'enemy-bullets
                          #:rank 5
                          #:capacity 1000
                          #:texture-atlas enemy-bullet-atlas))
         (explosions (make <particles>
                       #:name 'explosions
                       #:rank 3
                       #:particles
                       (make-particles 1000
                                       #:texture (asset-ref explosion-texture)
                                       #:end-color (make-color 1.0 1.0 1.0 0.0)
                                       #:speed-range (vec2 0.5 5.0)
                                       #:lifetime 12)))
         (ui (make <node-2d>
               #:name 'ui
               #:rank 999)))
    (set! (rank player) 1)
    (attach-to game
               ;; (make <sprite>
               ;;   #:name 'clouds
               ;;   #:rank 0
               ;;   #:texture clouds)
               player
               player-bullets
               (make <node-2d>
                 #:name 'enemies
                 #:rank 4)
               explosions
               enemy-bullets
               ui)
    ;; Setup UI elements
    ;; TODO: Move this out of here.
    (attach-to ui
               (make <label>
                 #:name 'score
                 #:position (vec2 2.0 242.0)
                 #:vertical-align 'top)
               (make <label>
                 #:name 'chain
                 #:position (vec2 158.0 242.0)
                 #:align 'right
                 #:vertical-align 'top)
               (make <label>
                 #:name 'energy
                 #:position (vec2 158.0 2.0)
                 #:align 'right)
               (make <label>
                 #:name 'lives
                 #:position (vec2 2.0 2.0)))))

(define-method (start-stage (game <game>))
  (update-ui game)
  (play-stage-1 game))

(define-method (stop-stage (game <game>))
  (cancel-script (stage-script game)))

(define-method (update-ui (game <game>))
  (set! (text (& game ui score))
    (format #f "~7,'0d" (score (& game player))))
  (set! (text (& game ui chain))
    (format #f "~a CHAIN (~a)"
            (let ((n (chain (& game player))))
              (if (< n 9) (number->string n) "MAX"))
            (list->string
             (map (lambda (polarity)
                    (if (eq? polarity 'white)
                        #\W
                        #\B))
                  (chain-progress (& game player))))))
  (set! (text (& game ui energy))
    (format #f "ENERGY ~d" (quotient (energy (& game player)) 10)))
  (set! (text (& game ui lives))
    (format #f "SHIP x~d" (max (- (lives (& game player)) 1) 0))))

(define-method (explode (game <game>) (actor <actor>))
  (let* ((p (position actor))
         (emitter (make-particle-emitter (make-rect (- (vec2-x p) 8.0)
                                                    (- (vec2-y p) 8.0)
                                                    16.0 16.0)
                                         8 5)))
    (add-particle-emitter (particles (& game explosions)) emitter)))

(define-method (update (game <game>) dt)
  (let ((refresh-ui? #f)
        (player (& game player)))
    ;; enemy -> player bullet collision
    ;; enemy -> player collision
    (for-each (lambda (enemy)
                (cond
                 ((and (collide (& game player-bullets) enemy)
                       (dead? enemy))
                  (on-kill player enemy)
                  (fire-parting-shots-maybe enemy player)
                  (explode game enemy)
                  (stop-scripts enemy)
                  (detach enemy)
                  (set! refresh-ui? #t))
                 ((collide player enemy)
                  (set! refresh-ui? #t))))
              (children (& game enemies)))
    ;; player -> enemy bullet collision
    (when (collide (& game enemy-bullets) (& game player))
      (set! refresh-ui? #t))
    (when (game-over? game)
      (steer (& game player) #f #f #f #f)
      (stop-shooting (& game player)))
    (when refresh-ui?
      (update-ui game))
    (next-method)))

(define-method (spawn-enemy (game <game>) enemy)
  (set! (bullet-field enemy) (& game enemy-bullets))
  (attach-to (& game enemies) enemy))

(define-method (steer-player (game <game>) up? down? left? right?)
  (when (player-control? game)
    (steer (& game player) up? down? left? right?)))

(define-method (start-player-shooting (game <game>))
  (when (player-control? game)
    (start-shooting (& game player))))

(define-method (stop-player-shooting (game <game>))
  (when (player-control? game)
    (stop-shooting (& game player))))

(define-method (toggle-player-polarity (game <game>))
  (when (player-control? game)
    (toggle-polarity (& game player))))

(define-method (fire-player-homing-missiles (game <game>))
  (when (player-control? game)
    (fire-homing-missiles (& game player) (children (& game enemies)))
    (update-ui game)))

(define-method (game-over? (game <game>))
  (dead? (& game player)))

(define-method (play-stage-1 game)
  (set! (stage-script game)
    (run-script game
      (do-intro game)
      (do-tutorial game)
      (do-phase-1 game)
      (do-phase-2 game)
      (toratsugumi-sweep game '((() (white white white) 45)
                                ((black black black) () 60)))
      (interstitial game)
      (phase-3 game)
      (toratsugumi-sweep game '((() (white white white) 45)
                                ((black black black) () 60)))
      ;; utatsugumi wall
      (toratsugumi-sweep game '((() (white white white) 45)
                                ((black black black) () 60)))
      ;; torafuzuku attack
      (toratsugumi-sweep
       game
       '(((white white black) (white black black) 30)
         ((white black black) (white white black) 30)
         ((black white white) (black black white) 30)
         ((white white black) (white black black) 180)))
      ;; small renjyaku attack
      ;; utatsugumi ring
      ;; toratsugumi line
      ;; ajisashi attack
      ;; toratsugumi lines
      ;; toratsugumi wall
      ;; eboshidori intro
      ;; eboshidori phase 1
      ;; eboshidori phase 2
      ;; eboshidori phase 3
      ;; eboshidori phase 4
      (do-win game))))

(define-method (do-intro (game <game>))
  (hide (& game ui))
  (set! (bounds-check? (& game player)) #f)
  (teleport (& game player) 80.0 -24.0)
  (move-to (& game player) 80.0 32.0 50)
  (set! (bounds-check? (& game player)) #t)
  ;;(steer (& game player) #f #f #f #f)
  (set! (player-control? game) #t)
  (show (& game ui)))

(define-method (do-tutorial (game <game>))
  (define* (instruct text continue? #:optional (post-delay 60))
    (let ((instructions (make <label>
                          #:text text
                          #:align 'center
                          #:vertical-align 'center
                          #:position (vec2 80.0 120.0))))
      (attach-to (& game ui) instructions)
      (while (not (continue?))
        (sleep 10))
      (sleep post-delay)
      (detach instructions)
      (sleep 60)))
  (unless (skip-tutorial? game)
    (sleep 30)
    (instruct "use arrow keys to move"
              (let ((v (velocity (& game player))))
                (lambda ()
                  (not (and (= (vec2-x v) 0.0)
                            (= (vec2-y v) 0.0))))))
    (instruct "press Z to shoot"
              (lambda ()
                (shooting? (& game player))))
    (instruct "press X to change energy"
              (let ((starting-polarity (polarity (& game player))))
                (lambda ()
                  (not (eq? (polarity (& game player)) starting-polarity)))))
    (instruct "avoid opposite energy" (const #t) 120)
    (instruct "absorb same energy" (const #t) 120)
    (add-energy (& game player) 120)
    (update-ui game)
    (instruct "press C to release energy"
              (lambda ()
                (zero? (energy (& game player)))))
    (instruct "shoot opposite energy" (const #t) 90)
    (instruct "deal 2x damage" (const #t) 90)
    (set! (invincible? (& game player)) #t)
    (spawn-enemy game (make-utatsugumi 'white 48.0 150.0))
    (spawn-enemy game (make-utatsugumi 'white 80.0 150.0))
    (spawn-enemy game (make-utatsugumi 'white 112.0 150.0))
    (instruct "destroy 3 of same energy"
              (lambda ()
                (null? (children (& game enemies)))))
    (spawn-enemy game (make-utatsugumi 'black 48.0 150.0))
    (spawn-enemy game (make-utatsugumi 'black 80.0 150.0))
    (spawn-enemy game (make-utatsugumi 'black 112.0 150.0))
    (instruct "repeat for chain bonus"
              (lambda ()
                (null? (children (& game enemies)))))
    (set! (score (& game player)) 0)
    (set! (chain (& game player)) 0)
    (set! (chain-progress (& game player)) '())
    (set! (max-chain (& game player)) 0)
    (set! (energy (& game player)) 0)
    (set! (invincible? (& game player)) #f)
    (update-ui game)
    (instruct "get ready!" (const #t) 120)
    (set! (skip-tutorial? game) #t)))

(define-method (do-phase-1 (game <game>))
  (define (utatsugumi-sweep x dir polarity)
    (let ((speed 3.0))
      (let loop ((i 0))
        (when (< i 6)
          (let ((utatsugumi (make-utatsugumi polarity x 260.0)))
            (spawn-enemy game utatsugumi)
            (set-vec2! (velocity utatsugumi)
                       (* (cos (* pi 1.5)) speed)
                       (* (sin (* pi 1.5)) speed))
            (run-script utatsugumi
             (sleep 5)
             (let loop ((i 0))
               (when (< i 25)
                 (let ((theta (+ (* pi 1.5)
                                 (* dir i (/ (* pi .5) 25.0)))))
                   (set-vec2! (velocity utatsugumi)
                              (* (cos theta) speed)
                              (* (sin theta) speed))
                   (sleep 3)
                   (loop (+ i 1)))))
             (sleep 60)
             (detach utatsugumi))
            (sleep 15))
          (loop (+ i 1))))))
  (utatsugumi-sweep 140.0 -1.0 'white)
  (sleep 15)
  (utatsugumi-sweep 20.0 1.0 'black)
  (sleep 15)
  (utatsugumi-sweep 140.0 -1.0 'white)
  (sleep 15)
  (utatsugumi-sweep 20.0 1.0 'black)
  (sleep 60))

(define-method (do-phase-2 (game <game>))
  (define (spawn-toratsugumi polarity row column start-x dx-factor)
    (let ((toratsugumi (make-toratsugumi polarity
                                         (+ start-x (* column 20.0))
                                         (+ 240.0 (* row 20.0)))))
      (spawn-enemy game toratsugumi)
      (run-script toratsugumi
        (set-vec2! (velocity toratsugumi) (* dx-factor 1.8) -1.4)
        (sleep 80)
        (tween 10 (* dx-factor 1.8) 0.0
               (lambda (dx)
                 (set-vec2! (velocity toratsugumi) dx -0.5)))
        (sleep 10)
        (tween 60 0.0 (* dx-factor -1.8)
               (lambda (dx)
                 (set-vec2-x! (velocity toratsugumi) dx)))
        (sleep 60)
        (detach toratsugumi))))
  (let row-loop ((row 0))
    (when (< row 6)
      (let column-loop ((column 0))
        (when (< column 3)
          (spawn-toratsugumi 'white row column 160.0 -1.0)
          (spawn-toratsugumi 'black row (- column) 0.0 1.0)
          (column-loop (+ column 1))))
      (sleep 3)
      (row-loop (+ row 1))))
  (sleep 180))

(define-method (toratsugumi-sweep (game <game>) pattern)
  (define (sweep x dir polarities)
    (script
     (let loop ((polarities polarities))
       (match polarities
         (() #t)
         ((polarity . rest)
          (let ((toratsugumi (make-toratsugumi polarity x 250.0))
                (speed 2.0))
            (spawn-enemy game toratsugumi)
            (run-script toratsugumi
              (script
               (tween 60 speed 4.0
                      (lambda (s)
                        (set! speed s))))
              (script
               (tween 50 (* pi 1.5) (+ (* pi 1.5) (* dir .35 pi))
                      (lambda (theta)
                        (set-vec2! (velocity toratsugumi)
                                   (* (cos theta) speed)
                                   (* (sin theta) speed)))
                      #:ease ease-out-sine)
               (sleep 60)
               (detach toratsugumi)))
            (sleep 8)
            (loop rest)))))))
  (let loop ((pattern pattern))
    (match pattern
      (() #t)
      (((left right dt) . rest)
       (sweep 130.0 -1.0 right)
       (sweep 30.0 1.0 left)
       (sleep dt)
       (loop rest)))))

(define-method (phase-3 (game <game>))
  ;; Renjyaku attack
  (define (fly-away renjyaku dir)
    (let ((theta (if (= dir 1.0) (* pi 1.1) (* 2pi .9))))
      (tween 10 0.0 2.0
             (lambda (speed)
               (set-vec2! (velocity renjyaku)
                          (* (cos theta) speed)
                          (* (sin theta) speed))))))
  (define* (triangle polarities ox oy dir #:optional (proc (lambda (ren) (sleep 20))))
    (for-each (lambda (polarity x y)
                (let* ((sx (+ 80.0 (* dir 96.0)))
                       (sy 256.0)
                       (dx (- x sx))
                       (dy (- y sy))
                       (ren (make-renjyaku polarity sx sy)))
                  (spawn-enemy game ren)
                  (run-script ren
                    (set-vec2! (velocity ren) (/ dx 40.0) (/ dy 40.0))
                    (sleep 40)
                    (set-vec2! (velocity ren) 0.0 0.0)
                    (proc ren)
                    (fly-away ren dir)
                    (sleep 90)
                    (detach ren)))
                (sleep 10))
              polarities
              (if (= dir 1.0)
                  (list (- ox 18.0) (+ ox 18.0) ox)
                  (list (+ ox 18.0) (- ox 18.0) ox))
              (list (- oy 14.0) (- oy 14.0) (+ oy 14.0))))
  (define (shoot-at-player ren)
    (sleep 45)
    (let* ((player (& game player))
           (ppos (position player))
           (epos (position ren))
           (bullets (& game enemy-bullets))
           (speed 4.0))
      (repeat 12
              (let* ((dx (- (vec2-x ppos) (vec2-x epos)))
                     (dy (- (vec2-y ppos) (vec2-y epos)))
                     (theta (+ (atan dy dx) (- (* (random:uniform) (/ pi 32.0)) (/ pi 64.0)))))
                (spawn-bullet bullets medium-dot (polarity ren)
                              (vec2-x epos) (- (vec2-y epos) 12.0)
                              (* (cos theta) speed)
                              (* (sin theta) speed)))
              (sleep 3))))
  (define (straight-down polarity x)
    (let ((ren (make-renjyaku polarity x 256.0)))
      (spawn-enemy game ren)
      (run-script ren
        (set-vec2! (velocity ren) 0.0 -3.0)
        (sleep 180)
        (detach ren))))
  (define (sine-wave polarity x dir)
    (let ((ren (make-renjyaku polarity x 256.0)))
      (spawn-enemy game ren)
      (run-script ren
        (let loop ((i 0.0))
          (when (< i (* 3.0 pi))
            (let ((dx (* (sin (* i dir)) 2.0))
                  (dy -1.0))
              (set-vec2! (velocity ren) dx dy))
            (sleep 3)
            (loop (+ i 0.1))))
        (detach ren))
      (run-script ren
        (sleep 30)
        (let ((pos (position ren))
              (bullets (& game enemy-bullets)))
          (forever
           (repeat 12
                   (spawn-bullet bullets medium-dot polarity
                                 (vec2-x pos) (- (vec2-y pos) 12.0)
                                 0.0 -3.0)
                   (sleep 4))
           (sleep 13))))))
  (triangle '(white white white) 70.0 130.0 1.0)
  (sleep 50)
  (triangle '(white white white) 90.0 140.0 -1.0)
  (sleep 50)
  (triangle '(black black black) 70.0 130.0 1.0)
  (sleep 50)
  (triangle '(black black black) 90.0 140.0 -1.0)
  (sleep 60)
  (triangle '(white white white) 40.0 200.0 1.0 shoot-at-player)
  (sleep 60)
  (triangle '(black black black) 120.0 200.0 -1.0 shoot-at-player)
  (sleep 60)
  (script
   (triangle '(black black white) 40.0 200.0 -1.0 shoot-at-player))
  (triangle '(black white white) 120.0 200.0 1.0 shoot-at-player)
  (sleep 60)
  (sine-wave 'white 20.0 1.0)
  (sleep 60)
  (sine-wave 'white 140.0 -1.0)
  (sleep 60)
  (sine-wave 'white 20.0 1.0)
  (sleep 60)
  (script
   (sleep 60)
   (straight-down 'black 20.0)
   (straight-down 'black 140.0)
   (sleep 30)
   (straight-down 'black 20.0)
   (straight-down 'black 140.0)
   (sleep 30)
   (straight-down 'black 20.0)
   (straight-down 'black 140.0))
  (sine-wave 'white 20.0 1.0)
  (sleep 40)
  (sine-wave 'black 140.0 -1.0)
  (sleep 40)
  (sine-wave 'white 20.0 1.0)
  (sleep 40)
  (sine-wave 'black 140.0 -1.0)
  (sleep 180))

(define-method (interstitial (game <game>))
  (let ((container (make <node-2d> #:rank 999)))
    (define (show-text text y)
      (let ((label (make <label>
                     #:text text
                     #:position (vec2 80.0 y)
                     #:align 'center
                     #:vertical-align 'center)))
        (attach-to container label)
        (run-script label
          (sleep 90)
          (move-by label 0.0 200.0 20))))
    (sleep 120)
    (attach-to game container)
    (show-text "- Lisparuga -" 120.0)
    (sleep 50)
    (show-text "Made for Lisp Game Jam" 104.0)
    (sleep 50)
    (show-text "Spring 2020" 88.0)
    (sleep 120)
    (detach container)
    (sleep 60)))

(define-method (do-win (game <game>))
  (set! (player-control? game) #f)
  (steer (& game player) #f #f #f #f)
  (stop-shooting (& game player))
  (hide (& game ui))
  (let ((battle-report (make <node-2d>
                         #:name 'battle-report
                         #:rank 999)))
    (define (add-row y name value)
      (attach-to battle-report
                 (make <label>
                   #:rank 999
                   #:text name
                   #:align 'left
                   #:position (vec2 16.0 y))
                 (make <label>
                   #:rank 999
                   #:text value
                   #:align 'left
                   #:position (vec2 96.0 y))))
    (let ((backdrop (make <filled-rect>
                      #:region (make-rect 0.0 0.0 160.0 240.0))))
      (attach-to battle-report backdrop)
      (attach-to game battle-report)
      (tween 45 (make-color 0.0 0.0 0.0 0.0) (make-color 0.0 0.0 0.0 0.8)
             (lambda (c)
               (set! (color backdrop) c))
             #:interpolate color-lerp))
    (sleep 30)
    (attach-to battle-report
               (make <label>
                 #:rank 999
                 #:text "STAGE CLEAR!"
                 #:align 'center
                 #:position (vec2 80.0 190.0)))
    (sleep 30)
    (attach-to battle-report
               (make <label>
                 #:rank 999
                 #:text "BATTLE REPORT"
                 #:align 'center
                 #:position (vec2 80.0 145.0)))
    (sleep 30)
    (add-row 120.0 "SCORE" (number->string (score (& game player))))
    (sleep 30)
    (add-row 100.0 "MAX CHAIN" (number->string (max-chain (& game player))))
    (sleep 30)
    (attach-to battle-report
               (make <label>
                 #:rank 999
                 #:text "press ENTER to play again"
                 #:position (vec2 80.0 40.0)
                 #:align 'center))
    (set! (complete? game) #t)))

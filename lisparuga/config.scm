;;; Lisparuga
;;; Copyright © 2020 David Thompson <dthompson2@worcester.edu>
;;;
;;; Lisparuga is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published
;;; by the Free Software Foundation, either version 3 of the License,
;;; or (at your option) any later version.
;;;
;;; Lisparuga is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Lisparuga.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; Global engine configuration.
;;
;;; Code:

(define-module (lisparuga config)
  #:export (developer-mode?
            asset-dir
            scope-asset))

(define developer-mode?
  (equal? (getenv "LISPARUGA_DEV_MODE") "1"))

(define asset-dir (getenv "LISPARUGA_ASSETDIR"))

(define (scope-asset file-name)
  (string-append asset-dir "/" file-name))

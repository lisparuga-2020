## Lisparuga
## Copyright © 2020 David Thompson <davet@gnu.org>
##
## Lisparuga is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published
## by the Free Software Foundation, either version 3 of the License,
## or (at your option) any later version.
##
## Lisparuga is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with Lisparuga.  If not, see <http://www.gnu.org/licenses/>.

GOBJECTS = $(SOURCES:%.scm=%.go)

nobase_mod_DATA = $(SOURCES) $(NOCOMP_SOURCES)
nobase_go_DATA = $(GOBJECTS)

# Make sure source files are installed first, so that the mtime of
# installed compiled files is greater than that of installed source
# files.  See
# <http://lists.gnu.org/archive/html/guile-devel/2010-07/msg00125.html>
# for details.
guile_install_go_files = install-nobase_goDATA
$(guile_install_go_files): install-nobase_modDATA

CLEANFILES = $(GOBJECTS)
EXTRA_DIST = $(SOURCES) $(NOCOMP_SOURCES)
GUILE_WARNINGS = -Wunbound-variable -Warity-mismatch -Wformat
SUFFIXES = .scm .go
.scm.go:
	$(AM_V_GEN)$(top_builddir)/pre-inst-env $(GUILE_TOOLS) compile $(GUILE_WARNINGS) -o "$@" "$<"

moddir=$(prefix)/share/guile/site/$(GUILE_EFFECTIVE_VERSION)
godir=$(libdir)/guile/$(GUILE_EFFECTIVE_VERSION)/site-ccache

bin_SCRIPTS =					\
  scripts/lisparuga

SOURCES = 					\
  lisparuga/config.scm				\
  lisparuga/inotify.scm				\
  lisparuga/asset.scm				\
  lisparuga/node.scm				\
  lisparuga/scene.scm				\
  lisparuga/repl.scm				\
  lisparuga/kernel.scm				\
  lisparuga/node-2d.scm				\
  lisparuga/transition.scm			\
  lisparuga/actor.scm				\
  lisparuga/bullets.scm				\
  lisparuga/player.scm				\
  lisparuga/enemy.scm				\
  lisparuga/game.scm				\
  lisparuga.scm

EXTRA_DIST += 					\
  COPYING

imagesdir = $(pkgdatadir)/images
dist_images_DATA = 				\
  assets/images/background.png			\
  assets/images/enemies.png			\
  assets/images/enemy-bullets.png		\
  assets/images/explosion.png			\
  assets/images/player-bullets.png		\
  assets/images/player.png

soundsdir = $(pkgdatadir)/sounds
dist_sounds_DATA =				\
  assets/sounds/energy-max.wav			\
  assets/sounds/explosion.wav			\
  assets/sounds/hit.wav				\
  assets/sounds/max-chain.wav			\
  assets/sounds/player-death.wav		\
  assets/sounds/player-missile.wav		\
  assets/sounds/player-shoot.wav

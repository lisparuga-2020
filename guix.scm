;;; Lisparuga
;;; Copyright © 2020 David Thompson <davet@gnu.org>
;;;
;;; Lisparuga is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published
;;; by the Free Software Foundation, either version 3 of the License,
;;; or (at your option) any later version.
;;;
;;; Lisparuga is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Lisparuga.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; Development environment for GNU Guix.
;;
;; To setup the development environment, run the following:
;;
;;    guix environment -l guix.scm
;;    ./bootstrap && ./configure;
;;
;; To build the development snapshot, run:
;;
;;    guix build -f guix.scm
;;
;; To install the development snapshot, run:
;;
;;    guix install -f guix.scm
;;
;;; Code:

(use-modules (ice-9 match)
             (srfi srfi-1)
             (guix build-system gnu)
             (guix download)
             (guix git-download)
             (guix licenses)
             (guix packages)
             (guix utils)
             (gnu packages)
             (gnu packages audio)
             (gnu packages autotools)
             (gnu packages pkg-config)
             (gnu packages texinfo)
             (gnu packages guile)
             (gnu packages gl)
             (gnu packages sdl)
             (gnu packages maths)
             (gnu packages mp3)
             (gnu packages image)
             (gnu packages xiph))

(define target-guile guile-3.0)

(define guile3.0-opengl
  (package
    (inherit guile-opengl)
    (inputs
     (map (match-lambda
            (("guile" _)
             `("guile" ,target-guile))
            (input input))
          (package-inputs guile-opengl)))
    (native-inputs
     (append (package-native-inputs guile-opengl)
             `(("autoconf" ,autoconf)
               ("automake" ,automake))))
    (arguments
     (substitute-keyword-arguments (package-arguments guile-opengl)
       ((#:phases phases)
        `(modify-phases ,phases
           (delete 'patch-makefile)
           (add-before 'bootstrap 'patch-configure.ac
             (lambda _
               ;; The Guile version check doesn't work for the 3.0
               ;; pre-release, so just remove it.
               (substitute* "configure.ac"
                 (("GUILE_PKG\\(\\[2.2 2.0\\]\\)") ""))
               (substitute* "Makefile.am"
                 (("\\$\\(GUILE_EFFECTIVE_VERSION\\)") "3.0")
                 (("ccache") "site-ccache"))
               #t))
           (replace 'bootstrap
             (lambda _
               (invoke "autoreconf" "-vfi")))))))))

(define guile-sdl2
  (let ((commit "dae8466030776f9e3afa851122705baaf09071a9"))
    (package
     (name "guile-sdl2")
     (version (string-append "0.5.0-1." (string-take commit 7)))
     (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "git://dthompson.us/guile-sdl2.git")
                    (commit commit)))
              (sha256
               (base32
                "12rrqdbscrsqpvwwakpv8k88cg53kj9q97diqmfic4hyz5skrgr3"))))
     (build-system gnu-build-system)
     (arguments
      '(#:make-flags '("GUILE_AUTO_COMPILE=0")
        #:phases
        (modify-phases %standard-phases
          (add-after 'unpack 'bootstrap
                     (lambda _
                       (zero? (system* "sh" "bootstrap")))))))
     (native-inputs
      `(("autoconf" ,autoconf)
        ("automake" ,automake)
        ("pkg-config" ,pkg-config)
        ("texinfo" ,texinfo)))
     (inputs
      `(("guile" ,target-guile)
        ("sdl2" ,sdl2)
        ("sdl2-image" ,sdl2-image)))
     (synopsis "Guile bindings for SDL2")
     (description "Guile-sdl2 provides pure Guile Scheme bindings to the
SDL2 C shared library via the foreign function interface.")
     (home-page "https://git.dthompson.us/guile-sdl2.git")
     (license lgpl3+))))

(define chickadee
  (let ((commit "b15a8b5be99df32e86af8bfb6f5dbdaacceda776"))
    (package
     (name "chickadee")
     (version (string-append "0.5.0-1." (string-take commit 7)))
     (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "git://dthompson.us/chickadee.git")
                    (commit commit)))
              (sha256
               (base32
                "17hba2mrvrdz91lgw92lf7qv4i6i2f5ypwrlr45fy4nljh52870c"))))
     (build-system gnu-build-system)
     (arguments
      '(#:make-flags '("GUILE_AUTO_COMPILE=0")
        #:phases
        (modify-phases %standard-phases
                       (add-after 'unpack 'bootstrap
                                  (lambda _
                                    (zero? (system* "sh" "bootstrap")))))))
     (native-inputs
      `(("autoconf" ,autoconf)
        ("automake" ,automake)
        ("pkg-config" ,pkg-config)
        ("texinfo" ,texinfo)))
     (inputs
      `(("guile" ,target-guile)
        ("libvorbis" ,libvorbis)
        ("mpg123" ,mpg123)
        ("openal" ,openal)))
     (propagated-inputs
      `(("guile-opengl" ,guile3.0-opengl)
        ("guile-sdl2" ,guile-sdl2)))
     (synopsis "Game development toolkit for Guile Scheme")
     (description "Chickadee is a game development toolkit for Guile
Scheme.  It contains all of the basic components needed to develop
2D/3D video games.")
     (home-page "https://dthompson.us/projects/chickadee.html")
     (license gpl3+))))

(let ((commit "0a7b302f04433f7e45ac694defa12d3ddda9e926"))
  (package
   (name "lisparuga")
   (version (string-append "0.1.0-1." (string-take commit 7)))
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url "git://dthompson.us/lisparuga-2020.git")
                  (commit commit)))
            (sha256
             (base32
              "044pss99lb1inniicrwwidaclcqzkblj9hccnwb9yrr75n7l2k14"))))
   (build-system gnu-build-system)
   (arguments
    '(#:make-flags '("GUILE_AUTO_COMPILE=0")
      #:phases
      (modify-phases %standard-phases
        (add-after 'unpack 'bootstrap
                   (lambda _
                     (zero? (system* "sh" "bootstrap")))))))
   (native-inputs
    `(("autoconf" ,autoconf)
      ("automake" ,automake)
      ("pkg-config" ,pkg-config)))
   (inputs
    `(("guile" ,target-guile)))
   (propagated-inputs
    `(("chickadee" ,chickadee)
      ("guile-sdl2" ,guile-sdl2)))
   (synopsis "Ikaruga demake for Spring Lisp Game Jam 2020")
   (description "Lisparuga is an Ikaruga demake made for the Spring Lisp
Game Jam 2020.")
   (home-page "https://davexunit.itch.io/lisparuga-2020")
   (license gpl3+)))
